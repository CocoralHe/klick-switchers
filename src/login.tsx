import app, { Component } from 'apprun';
import auth from './auth';

export default class loginComponent extends Component {
  state = { mode: 'login', returnUrl: null };

  constructor() {
    super();
    auth.onUnauthorized(this._onUnauthorized);
  }

  private _onUnauthorized = ((hash) => {

  }).bind(this);

  view = (state) => {
    if (state.mode === 'logout')
      return <div>Logging out...</div>;
    return <div>
      <h1>Welcome to Klick Switchers!</h1>
      <form id="login" action="api/login.php" method="POST" onsubmit={(ev) => {
        this.run('submit', ev);
        return false;
      }}>
        <input type="email" name="email" placeholder="your@email.address" required />
        {state.mode === 'signup' ? <input type="text" name="name" placeholder="Your name" required /> : ''}
        <input type="submit" />
      </form>
    </div>;
  }

  private _loginLoad = (state, returnUrl) => {
    state.returnUrl = returnUrl || state.returnUrl || '#mystuff';
    state.mode = 'login';
    return state;
  };

  update = {
    '/': this._loginLoad,
    '#': this._loginLoad,
    '#logout': state => {
      state.mode = 'logout';
      state.returnUrl = null;
      fetch('api/logout.php', {
        credentials: 'include',
        method: 'POST'
      }).then(function (resp) {
        auth.logout();
        location.hash = '';
      }.bind(this));
      return state;
    },
    'unauthorized': (state, ret) => {
      state.returnUrl = ret;
    },
    'submit': (state, ev) => {
      fetch(ev.target.getAttribute('action'), {
        credentials: 'include',
        method: ev.target.getAttribute('method'),
        body: new FormData(ev.target)
      }).then(function (resp) {
        if (resp.status === 401) {
          this.run('signup');
          return null;
        } else if (resp.status === 200) {
          return resp.json();
        }
      }.bind(this)).then(function (data: any) {
        if (data) {
          auth.login(data.user);
          location.href = state.returnUrl;
        }
      }.bind(this));
      return state;
    },
    'signup': state => {
      state.mode = 'signup';
      return state;
    }
  }
}
