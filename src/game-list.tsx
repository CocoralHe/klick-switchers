import app, {Component} from 'apprun';

function OneGame(attr){
  return <div class="game-medium">
    <img src="http://via.placeholder.com/100x120" alt="" class="game-thumb"/>
    <div className="game-details">
      <h2 class="game-title">The Legend of Zelda: Breath of the Wild for Nintendo Switch</h2>
      <p><small class="note-blue">Recalled</small></p>
      <p class="game-amout">2 copies, all taken</p>
      <p>10 people <a href="">waiting</a></p>
      <p>Next in line: <a class="note-red">Coral</a></p>
      <p>test:{attr.game}</p>
    </div>
  </div>
}

export default class GameListComponent extends Component {
  state = 'Game-List';

  view = (state) => {
    return <div>
      <h1><OneGame game={'here is one game'}/></h1>
    </div>
  }

  update = {
    '#Game-List': state => state,
  }
}


// to use this component in main.tsx
// import Game-List from './Game-List';
// const element = document.getElementById('my-app');
// new Game-List().start(element);
