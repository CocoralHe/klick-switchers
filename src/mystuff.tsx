import app, { Component } from 'apprun';
import auth from './auth';
import GameList from './game-list';
var hash = '#mystuff';

// new oneGame().start(element);

export default class mystuffComponent extends Component {
  state = 'mystuff';

  view = (state) => {
    var user = auth.getUser();
    if (!user.email) {
      app.run('/', hash);
      location.hash = '';
      return;
    }
    return <div>
      <GameList />
    </div>
  }

  update = {
    [hash]: state => state,
  }
}
