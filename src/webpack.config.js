const webpack = require('webpack');
const path = require('path');
module.exports = {
  entry: {
    'app': './main.tsx',
  },
  output: {
    filename: '[name].js'
    //,publicPath: 'http://localhost:8080/'
  },
  resolve: {
    extensions: ['.webpack.js', '.web.js', '.ts', '.tsx', '.js']
  },
  module: {
    rules: [
      { test: /.tsx?$/, loader: 'ts-loader' }
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: './js/jquery.slim.min',
      jQuery: './js/jquery.slim.min'
    })
  ],
  devServer: {
    proxy: {
      '/api': "http://localhost:8888"
    }
  }
}