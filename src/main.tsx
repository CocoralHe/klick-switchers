import $ = require('./js/jquery.slim.min.js');
import app from 'apprun';
import login from './login';
import about from './about';
import mystuff from './mystuff';
import name from './name';

const element = document.getElementById('my-app');

new name().start(document.querySelector<HTMLElement>('.js-name'));

new login().start(element);
new mystuff().start(element);
new about().start(element);

app.on('#', () => void 0);
app.on('//', route => {
	const links = $('nav a').each((el) => {
		$(el).removeClass('current');
	});
	console.log(route);
	$(`nav a[href$="${route}"]`).addClass('current');
});