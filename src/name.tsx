import app, { Component } from 'apprun';
import auth from './auth';

var nameComponents = [];

export default class nameComponent extends Component {
  state = '';

  constructor() {
    super();
    nameComponents.push(this);
    auth.onLogout(this._loggedOut);
    auth.onLogin(this._loggedIn);
  }

  private _loggedIn = (() => this.run('logged-in')).bind(this);
  private _loggedOut = (() => this.run('logged-out')).bind(this);

  view = (state) => {
    if (!state)
      return;
    return <strong>Hi {state}</strong>
  }

  update = {
    'logged-in': (state) => auth.getUser().name,
    'logged-out': state => ''
  }
}