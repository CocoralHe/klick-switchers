var curUser: any = {};

var loginHandlers = [];
var logoutHandlers = [];
var unauthorizedHandlers = [];

export default {
	login(user: any) {
		this.logout();
		if (user && user.email && user.name) {
			curUser.email = user.email;
			curUser.name = user.name;
			for (var x = 0; x < loginHandlers.length; x++)
				loginHandlers[x]();
		}
	},
	logout() {
		if (curUser.email || curUser.name) {
			for (var x = 0; x < logoutHandlers.length; x++)
				logoutHandlers[x]();
			delete curUser.email;
			delete curUser.name;
		}
	},
	getUser(requestHash?: String) {
		if (!!requestHash && (!curUser.email || !curUser.name)) {
			for (var x = 0; x < unauthorizedHandlers.length; x++)
				unauthorizedHandlers[x](requestHash);
		}
		return curUser;
	},
	onUnauthorized(fn: Function) {
		unauthorizedHandlers.push(fn);
	},
	onLogin(fn: Function) {
		loginHandlers.push(fn);
	},
	onLogout(fn: Function) {
		logoutHandlers.push(fn);
	}
};