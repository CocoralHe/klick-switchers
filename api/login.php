<?php
/*
responds to login and signup requests

If login is called with no parameters:
	Checks if user is logged in. If so retrieves name and sends back user details. Otherwise sends back an unauthorized response.
*/

define('ALLOW_ANONYMOUS', true);
require_once('_api.php');

if($_SERVER['REQUEST_METHOD'] === 'POST'){
	if(empty($_POST['email']))
		dataError();
	$email = $_POST['email'];
	if(preg_match('/.@.+\../', $email) !== 1)
		dataError();

	$email = strtolower($email);

	if(!empty($_POST['name']))
		$name = $_POST['name'];
}else if($_SERVER['REQUEST_METHOD'] === 'GET'){
	if(empty($user))
		unauthorizedError();
	else
		$email = $user;
}else{
	unexpectedMethodError();
}

function login(){
	global $email, $name;
	setcookie('user', $email, 0, NULL, NULL, false, true);
	echo json_encode(array(
		'error' => NULL,
		'user' => array(
			'email' => $email,
			'name' => $name
		)
	));
}
function signup(){
	global $email, $name;
	dbExecuteStatement('INSERT INTO Users(Email, [Name], Registered) VALUES (:email, :name, datetime(\'now\'))', array(
		':email' => $email,
		':name' => $name
	));
	login();
}
//First try to login
$result = dbExecuteStatement('SELECT [name] FROM Users WHERE Email=:email', array(':email' => $email))->fetchArray();
if($result !== FALSE){
	//Found user, login!
	$name = $result[0];
	login();
}else if(isset($name)){
	//User not found, but a name was submitted, register and then login!
	signup();
}else{
	errorOutput(401, 401, 'Unrecognized user email! Please sign up.');
}