<?php

require_once(__DIR__ . '/../_api.php');

function populateUserGameCopies(&$res){
	global $user;
	$results = dbExecuteStatement('SELECT GameCopy_ID as [gcid],Game_ID as [gid],Owner_Email as [owner],Holder_Email as [holder],Next_Email as [next] FROM GameCopies WHERE Owner_Email=:user OR Holder_Email=:user',array(
		':user' => $user
	));
	$res['owned'] = array();
	$res['held'] = array();
	while(($row = $results->fetchArray(SQLITE3_ASSOC)) !== FALSE){
		if($row['holder'] === $user)
			$res['held'][] = $row;
		if($row['owner'] === $user)
			$res['owned'][] = $row;
	}
	return $res;
}

function populateQueuedEntries($res){
	global $user;
	$entries = array();
	$res['queued'] = array();
	return $res;
}
$details = isset($_GET['details']) && $_GET['details'] === '1';

$res = array();

populateUserGameCopies($res);
populateQueuedEntries($res);

echo json_encode(array('data' => $res));