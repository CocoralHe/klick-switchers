<?php

define('ALLOW_ANONYMOUS', TRUE);
require_once(__DIR__.'/../_api.php');

if(empty($_GET['q']))
	errorOutput(400, 400, 'Game search requires a query parameter.');

define('GAME_ESHOP_LINK_PREFIX', 'https://www.nintendo.com/games/detail/');

$q = $_GET['q'];

$nsres = file_get_contents('https://www.nintendo.com/json/content/get/game/list/filter/subset?qtitlelike='.urlencode($q).'&qsortBy=releaseDate&qdirection=descend&qhardware=Nintendo%20Switch');

$nsres = json_decode($nsres);

$res = array(
	'data' => array(
		'games' => array(),
		'total' => $nsres->total
	)
);
foreach($nsres->game as $game){
	$gameData = array(
		':id' => $game->id,
		':title' => $game->title,
		':eshop' => GAME_ESHOP_LINK_PREFIX . $game->id,
		':release_date' => $game->release_date,
		':publisher' => $game->publisher,
		':art' => $game->front_box_art->url
	);
	dbExecuteStatement('REPLACE INTO Games(Id,Title,ReleaseDate,Publisher,Art) VALUES (:id,:title,:release_date,:publisher,:art)', $gameData);
	foreach(array_keys($gameData) as $key){
		$gameData[substr($key, 1)] = $gameData[$key];
		unset($gameData[$key]);
	}
	$res['data']['games'][] = $gameData;
}


echo json_encode($res);