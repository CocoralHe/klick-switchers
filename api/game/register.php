<?php

require_once(__DIR__ . '/../_api.php');

try{
	$res = dbExecuteStatement('INSERT INTO GameCopies(Game_ID,Owner_Email,Holder_Email) VALUES (:gid,:user,:user)',array(
		':gid' => $_POST['gid'],
		':user' => $user
	), FALSE);
	$success = TRUE;
}catch(Exception $ex){
	$success = FALSE;
	if(strpos($ex->getMessage(), 'FOREIGN KEY constraint failed') !== FALSE){
		dataError();
	}
	throw $ex;
}

echo json_encode(array('success' => $success));