<?php
//All API preprocessing code will live in this file

require_once(__DIR__ . '/_core.php');

if(!defined('ALLOW_ANONYMOUS'))
	define('ALLOW_ANONYMOUS', FALSE);

function databaseError(){
	errorOutput(500, 500, 'A database error was encountered!');
}

function dbExecuteStatement($query, $bindings, $exitOnFail = TRUE){
	global $db;
	$stmt = $db->prepare($query);
	foreach ($bindings as $name => $value){
		$stmt->bindValue($name, $value);
	}
	if($exitOnFail){
		try{
			$result = $stmt->execute();
		}catch(Exception $ex){
		}
		if($result === FALSE)
			databaseError();
	}else{
		$result = $stmt->execute();
	}
	return $result;
}

if (isset($_COOKIE['user'])){
	$user = strtolower($_COOKIE['user']);
}

$data_file = __DIR__ . '/../db/prod.sqlite';
if(!file_exists($data_file)){
	$data_file = __DIR__ . (isset($_GET['test']) ? '/../db/test.sqlite' : '/../db/data.sqlite');
	$schema_file = __DIR__ . '/../db/schema.sqlite';
	if(!file_exists($data_file) || filemtime($schema_file) >= filemtime($data_file))
		copy($schema_file, $data_file);
}

$db = new SQLite3($data_file);
$db->enableExceptions(TRUE);
$db->exec('PRAGMA foreign_keys = ON;');
unset($data_file, $schema_file);

if(!ALLOW_ANONYMOUS){
	if(isset($user)){
		$result = dbExecuteStatement('SELECT [name] FROM Users WHERE Email=:email', array(':email' => $user))->fetchArray();
		if($result === FALSE){
			unset($user);
			unauthorizedError();
		}else{
			$user_name = $result[0];
		}
	}else{
		unauthorizedError();
	}
}
