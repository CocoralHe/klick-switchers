<?php

header('Content-Type: application/json');

function errorOutput($httpCode, $errorCode, $errorMessage){
	http_response_code($httpCode);

	echo json_encode(array(
		"error" => array(
			"code" => $errorCode,
			"msg" => $errorMessage
		)
	));
	exit(0);
}
function dataError(){
	errorOutput(400, 400, 'Data provided is invalid!');
}
function unauthorizedError(){
	errorOutput(401, 401, 'Not signed in!');
}
function unexpectedMethodError(){
	errorOutput(405, 405, 'Unexpected or unallowed request method used.');
}
