"Runs the test suite"
# pylint: disable=C0103,W0603

import sys
import inspect
import os
import shutil
import requests
import urllib
from colorama import init, Fore, Style
init()

counts = {
    'func': {
        'success': 0,
        'failure': 0,
        'warning': 0
    },
    'suite': {
        'success': 0,
        'failure': 0,
        'warning': 0
    },
    'total': {
        'success': 0,
        'failure': 0,
        'warning': 0
    }
}


def noop():
    "NO-OP"


def color_text(bright, color, text, text_only_prefix='', text_only_postfix=''):
    "Gets the ANSI style codes requested or an empty string if not in a TTY"
    if not sys.stdout.isatty():
        return text_only_prefix + text + text_only_postfix
    if bright:
        bright = Style.BRIGHT
    else:
        bright = Style.NORMAL
    if color is None:
        color = Fore.RESET
    return bright + color + text + Style.RESET_ALL


def inc_success():
    "Increments success counts"
    global counts
    counts['func']['success'] += 1
    counts['suite']['success'] += 1
    counts['total']['success'] += 1


def inc_warning():
    "Increments warning counts"
    global counts
    counts['func']['warning'] += 1
    counts['suite']['warning'] += 1
    counts['total']['warning'] += 1


def inc_failure():
    "Increments failure counts"
    global counts
    counts['func']['failure'] += 1
    counts['suite']['failure'] += 1
    counts['total']['failure'] += 1


def reset_func():
    "Increments success counts"
    global counts
    ret = (
        counts['func']['success'],
        counts['func']['warning'],
        counts['func']['failure']
    )
    counts['func']['success'] = 0
    counts['func']['warning'] = 0
    counts['func']['failure'] = 0
    return ret


def reset_suite():
    "Increments warning counts"
    global counts
    ret = (
        counts['suite']['success'],
        counts['suite']['warning'],
        counts['suite']['failure']
    )
    counts['suite']['success'] = 0
    counts['suite']['warning'] = 0
    counts['suite']['failure'] = 0
    return ret


def reset_total():
    "Increments failure counts"
    global counts
    ret = (
        counts['total']['success'],
        counts['total']['warning'],
        counts['total']['failure']
    )
    counts['total']['success'] = 0
    counts['total']['warning'] = 0
    counts['total']['failure'] = 0
    return ret


def test_factory(func):
    "Returns a function that allows a test function to report it's success or failure"
    func_module = inspect.getmodule(func)

    def test(passed):
        "Prints test results"
        global counts
        nonlocal func, func_module

        lineno = inspect.stack()
        for frame in lineno:
            if func_module == inspect.getmodule(frame[0]) and frame.function == func.__name__:
                lineno = frame.lineno
                break
        if passed is True:
            msg = color_text(False, Fore.GREEN, ' ' + str(lineno), text_only_postfix='P')
            inc_success()
        elif passed is False:
            msg = color_text(True, Fore.RED, ' ' + str(lineno), text_only_postfix='F')
            inc_failure()
        else:
            msg = color_text(True, Fore.YELLOW, ' ' + str(lineno), text_only_postfix='W')
            inc_warning()
        print(msg, end='')
    return test


def reset_db():
    "Resets the db to the initial state!"
    basedir = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'db')
    shutil.copy(os.path.join(basedir, 'schema.sqlite'), os.path.join(basedir, 'test.sqlite'))


def check_response(resp):
    "Checks that the response is marked and formatted as JSON"
    assert resp.headers['content-type'] == 'application/json'
    try:
        resp.json()  # Ensure body contains only JSON
    except:
        raise Exception('JSON expected, got: ' + resp.text)


def get(path, data=None, **kwargs):
    "HTTP Get JSON request (w/ checks)"
    resp = requests.get(path + '?test', data, **kwargs)
    check_response(resp)
    return resp


def post(path, data=None, **kwargs):
    "HTTP Post JSON request (w/ checks)"
    resp = requests.post(path + '?test', data, **kwargs)
    check_response(resp)
    return resp


def url_decode(url):
    "Decodes a URL encoded string"
    return urllib.parse.unquote(url, 'utf-8')


def run_tests():
    "Run the tests in the script directory"
    def print_totals(prefix, totals, skip_pass_only=True):
        "Prints the provided success, fail and warning totals"
        if skip_pass_only and totals[1] == 0 and totals[2] == 0:
            print()
            return
        msg = prefix + Fore.GREEN + 'PASS: ' + Style.BRIGHT + str(totals[0])
        if totals[2] > 0:
            msg += ', ' + Fore.RED + 'FAIL: ' + str(totals[2])
        if totals[1] > 0:
            msg += ', ' + Fore.YELLOW + 'WARN: ' + str(totals[1])
        msg += Style.RESET_ALL
        print(msg)

    restrict_module = None
    restrict_test = None
    if __name__ == '__main__':
        if len(sys.argv) > 3:
            raise Exception("Too many arguments!")
        if len(sys.argv) > 1:
            restrict_module = sys.argv[1]
        if len(sys.argv) > 2:
            restrict_test = sys.argv[2]

    suite_module = inspect.getmodule(run_tests)
    modules = [inspect.getmodule(inspect.stack()[1][0])]
    if modules[0] == suite_module:
        script_dir = os.path.dirname(os.path.realpath(__file__))
        os.listdir(script_dir)
        modules = [__import__(os.path.splitext(f)[0]) for f in os.listdir(script_dir) if f.endswith('.py') and f != __file__ and os.path.isfile(os.path.join(script_dir, f))]

    for module in modules:
        if restrict_module is not None and not restrict_module == module.__name__:
            continue
        reset_suite()
        #print(Style.BRIGHT + module.__name__ + Style.RESET_ALL)
        functions = [member[1] for member in inspect.getmembers(module) if inspect.isfunction(member[1]) and member[1].__name__.startswith('test_')]
        for func in functions:
            if restrict_test is not None and not restrict_test == func.__name__[5:]:
                continue
            reset_func()
            reset_db()
            print(module.__name__ + ' ' + Style.BRIGHT + func.__name__[5:] + Style.RESET_ALL, end='')
            func(test_factory(func))
            print_totals(' ', reset_func(), True)
        print_totals(Style.BRIGHT + module.__name__ + ' ' + Style.NORMAL, reset_suite(), True)
    print_totals('', reset_total(), False)


if __name__ == '__main__':
    run_tests()
