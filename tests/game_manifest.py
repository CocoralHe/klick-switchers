"""Game Manifest API tests"""

import suite as s
import login


def manifest(**kwargs):
    "Gets the manifest for the currently signed in user"
    return s.get('http://localhost:8080/api/game/manifest.php', **kwargs)


def test_auth(test):
    "Tests inaccessibility if not signed in"
    test(manifest().status_code == 401)


def test_pass(test):
    "Tests success result if signed in"
    test(manifest(cookies=login.signup().cookies).status_code == 200)
