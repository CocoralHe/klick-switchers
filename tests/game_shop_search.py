"""Game Shop Search API tests"""

import suite as s


def search(query):
    "Requests search results from nintendo shop search"
    return s.get('http://localhost:8080/api/game/shop_search.php', {'q': query})


def search_exact(title):
    "Searchs for a game with the EXACT title and gets it's ID"
    results = search(title).json()
    title = title.lower()
    return next((g for g in results['data']['games'] if g['title'].lower() == title), None)['id']


def get_zelda_id():
    "Gets the known game ID for a known game"
    return 'GHbaBYuv3zqQeW1CVQlmTSehZ45KTV78'


def test_search(test):
    "Tests that search returns expected output"
    resp = search('zelda')
    test(resp.status_code == 200)


def test_no_query(test):
    "Tests that an error is returned if no query is passed to search"
    resp = search('')
    test(resp.status_code == 400)
    test(search(None).status_code == 400)


def test_zelda(test):
    "Tests a known game to check it's game ID hasn't changed"
    game_id = search_exact('The Legend of Zelda: Breath of the Wild')
    test(game_id == get_zelda_id())
