"""Game Register API tests"""

import suite as s
import login
import game_manifest as gm
import game_shop_search as gs


def register_game(game_id, **kwargs):
    "Register a copy of a game to logged in user"
    return s.post("http://localhost:8080/api/game/register.php", {'gid': game_id}, **kwargs)


def test_auth(test):
    "Test can not access if not signed in"
    resp = register_game(gs.search_exact('The Legend of Zelda: Breath of the Wild'))
    test(resp.status_code == 401)


def test_invalid_gid(test):
    "Test submitting a non-existent game ID"
    login_creds = login.signup().cookies
    resp = register_game('abc', cookies=login_creds)
    test(resp.status_code == 400)


def test_register_game(test):
    "Test can register game"
    login_creds = login.signup().cookies
    resp = register_game(gs.search_exact('The Legend of Zelda: Breath of the Wild'), cookies=login_creds)
    test(resp.status_code == 200)
    resp = gm.manifest(cookies=login_creds)
    test(resp.status_code == 200)
