"""Login API tests"""

import suite as s


def test_ping_not_logged_in(test):
    "Test login pinging"
    resp = ping()
    test(resp.status_code == 401)


def test_ping_non_existent_user(test):
    "Test pinging with user that does not exist"
    resp = ping(cookies={'user': 'does@not.exist'})
    test(resp.status_code == 401)


def test_no_user(test):
    "Test login with no user"
    resp = s.post("http://localhost:8080/api/login.php")
    test(resp.status_code == 400)


def test_non_existent_user(test):
    "Test login with user that does not exist"
    resp = login(email='does@not.exist')
    test(resp.status_code == 401)


def ping(**kwargs):
    "Pings to see if user is logged in"
    return s.get("http://localhost:8080/api/login.php", **kwargs)


def signup(email='user@user.com', name='User1', **kwargs):
    "Signs up a user with the given name"
    return s.post("http://localhost:8080/api/login.php", {'email': email, 'name': name}, **kwargs)


def login(email='user@user.com', **kwargs):
    "Logs in user with given email"
    return s.post("http://localhost:8080/api/login.php", {'email': email}, **kwargs)


def test_signup(test):
    "Test creating new user"
    resp = signup(email='user@user.com', name='User1')
    test(resp.status_code == 200)
    test(resp.json()['user']['name'] == 'User1')
    test(s.url_decode(resp.cookies['user']) == 'user@user.com')
    # Check that if already signed up, don't override username
    resp = signup(email='user@user.com', name='User2')
    test(resp.status_code == 200)
    test(resp.json()['user']['name'] == 'User1')
    test(s.url_decode(resp.cookies['user']) == 'user@user.com')
    # Signup if already signed in replaces currently logged in user
    resp = signup(email='user2@user.com', name='User2', cookies=resp.cookies)
    test(resp.status_code == 200)
    test(resp.json()['user']['name'] == 'User2')
    test(s.url_decode(resp.cookies['user']) == 'user2@user.com')


def test_existing_user(test):
    "Test can login"
    signup(email='user@user.com')
    resp = login(email='user@user.com')
    test(resp.status_code == 200)
    test(s.url_decode(resp.cookies["user"]) == 'user@user.com')
    # Log in user when they are already logged in, keeps user logged in
    resp = login(email='user@user.com', cookies=resp.cookies)
    test(resp.status_code == 200)
    test(s.url_decode(resp.cookies["user"]) == 'user@user.com')
    # Log in non-existent user when already logged in, does not return a user set-cookie, and returns an error
    resp = login(email='does@not.exist', cookies=resp.cookies)
    test(resp.status_code == 401)
    test(not 'user' in resp.cookies)


def test_not_an_email(test):
    "Test when invalid email submitted"
    resp = login(email='u')
    test(resp.status_code == 400)
    test(not 'user' in resp.cookies)
