"""Logout API tests"""

import suite as s
import login
import requests


def logout(**kwargs):
    "Logs out the current user"
    return s.get('http://localhost:8080/api/logout.php', **kwargs)


def logout_post(**kwargs):
    "Logs out the current user"
    return s.post('http://localhost:8080/api/logout.php', **kwargs)


def test_logout(test):
    "Test logout functionality"
    logged_in = login.signup()
    resp = logout(cookies=logged_in.cookies)
    test(not 'user' in resp.cookies)
    resp = logout_post(cookies=logged_in.cookies)
    test(not 'user' in resp.cookies)
